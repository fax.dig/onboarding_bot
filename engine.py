import requests
import telebot
import pandas as pd
from proceset import Proceset
from bot_keys import API_KEY, WEBHOOK_ID, BASE_URL, TELEBOT_KEY

bot = telebot.TeleBot(TELEBOT_KEY)

api_model = Proceset(api_key=API_KEY, base_url=BASE_URL)
#print('GUID: ',  api_model.get_guid())


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_photo(message.from_user.id,
                   photo="https://www.tadviser.ru/images/d/d1/%D0%98%D0%BD%D1%84%D0%BE%D0%BC%D0%B0%D0%BA%D1%81%D0%B8%D0%BC%D1%83%D0%BC_%D0%BB%D0%BE%D0%B3%D0%BE.jpg",
                   caption="Привет! Я онбординг-бот компании «Инфомаксимум». Я помогу тебе адаптироваться в корпоративной жизни и в рабочем функционале. Теперь введи свой идентификатор, который тебе выдал HR-менеджер, в формате: /verify идентификатор")


@bot.message_handler(commands=['verify'])
def handle_input(message):
    text = message.text.strip().split(" ")
    if len(text) == 1:
        bot.reply_to(message,
                     "Укажи свой идентификатор, который тебе выдал HR-менеджер, в формате: /verify идентификатор")
        return

    verification_ids = api_model.extract_data(f"SELECT verification_id FROM db_132.verification_log")[
        'verification_id'].values
    if text[1] not in str(verification_ids):
        bot.send_message(message.chat.id, "Идентификатор неверный!")
        return
    else:
        bot.send_photo(message.chat.id, photo="https://i.ibb.co/k5F1gx2/image1.png",
                       caption="Ты успешно авторизован! Теперь можешь начать онбординг через функцию /quiz")
        query = 'mutation{automation{script{execute_with_data(id: 1158, data: \"[{\\\"verification_id\\\": ' + str(
            text[1]) + ', \\\"tg_id\\\": ' + str(message.from_user.id) + '}]\"){id, is_running}}}}'
        api_model.send_request(query)
        return


@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    cb_data = call.data.split('_')
    data = pd.DataFrame(columns=['user_id', 'question_id', 'answer'],
                        data=[[int(call.from_user.id), int(cb_data[0]), int(cb_data[1])]])
    requests.post(BASE_URL + "webhook/" + WEBHOOK_ID, json=data.to_dict(orient="records"),
                  headers={"Content-Type": "application/json"})
    query = 'mutation{automation{script{execute_with_data(id: 1153, data: \"[{\\\"chat_id\\\": ' + str(
        call.from_user.id) + ', \\\"id_question\\\": ' + str(int(cb_data[0]) + 1) + '}]\"){id, is_running}}}}'
    api_model.send_request(query)
    # Сохраняем в таблицу значение отправленного вопроса
    query = 'mutation{automation{script{execute_with_data(id: 1159, data: \"[{\\\"chat_id\\\": ' + str(
        call.from_user.id) + ', \\\"id_question\\\": ' + str(int(cb_data[0]) + 1) + '}]\"){id, is_running}}}}'
    api_model.send_request(query)
    bot.edit_message_reply_markup(call.from_user.id, call.message.id)


@bot.message_handler(commands=['quiz'])
def quiz_start(message):
    telegram_ids = api_model.extract_data(f"SELECT telegram_id FROM db_132.verification_log")[
        'telegram_id'].values
    if str(message.from_user.id) in str(telegram_ids):
        query = 'mutation{automation{script{execute_with_data(id: 1153, data: \"[{\\\"chat_id\\\": ' + str(
            message.from_user.id) + ', \\\"id_question\\\": ' + str(int(0) + 1) + '}]\"){id, is_running}}}}'
        api_model.send_request(query)
    else:
        bot.send_message(message.chat.id, "Ты не авторизован! Пройди авторизацию командой /verify")


@bot.message_handler(func=lambda message: True)
def text_answer_handler(message):
    chat_id = message.from_user.id
    try:
        active_question = api_model.extract_data(
            "SELECT active_question FROM db_132.verification_log WHERE telegram_id = " + str(chat_id))[
            'active_question'].values[0]
    except:
        # Пользователь не найден в базе
        return
    try:
        question_type = api_model.extract_data(
            f"SELECT question_type FROM db_132.bz_questions WHERE id_question = " + str(active_question))[
            'question_type'].values[0]
    except:
        # Вопрос не найден в базе
        return
    if int(question_type) == 2:
        bot.send_message(message.chat.id, "Ответ принят!")

        data = pd.DataFrame(columns=['user_id', 'question_id', 'answer'],
                            data=[[int(message.from_user.id), int(active_question), str(message.text)]])
        requests.post(BASE_URL + "webhook/" + WEBHOOK_ID, json=data.to_dict(orient="records"),
                      headers={"Content-Type": "application/json"})

        # Запрашиваем новый вопрос
        query = 'mutation{automation{script{execute_with_data(id: 1153, data: \"[{\\\"chat_id\\\": ' + str(
            message.from_user.id) + ', \\\"id_question\\\": ' + str(
            int(active_question) + 1) + '}]\"){id, is_running}}}}'
        api_model.send_request(query)
        # Сохраняем в таблицу значение отправленного вопроса
        query = 'mutation{automation{script{execute_with_data(id: 1159, data: \"[{\\\"chat_id\\\": ' + str(
            message.from_user.id) + ', \\\"id_question\\\": ' + str(
            int(active_question) + 1) + '}]\"){id, is_running}}}}'
        api_model.send_request(query)
    else:
        bot.send_message(message.from_user.id, "Некорректный ввод")
    return


try:
    bot.polling(none_stop=True, interval=0)
except Exception as error:
    print(error)